﻿using System;
namespace mmkorea.Models
{
    public class User
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Email { get; set; }

        public int Phone { get; set; }

        public Role Role { get; set; }
    }

    public enum Role
    {
        Admin,
        Maintainer,
        Superviser
    }
}
