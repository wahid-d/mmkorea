﻿using System;
namespace mmkorea.Models
{
    public class School
    {
        public int  ID { get; set; }

        public string Name { get; set; }

        public string UniqueCode { get; set; }

        public User Superviser { get; set; }
    }
}
