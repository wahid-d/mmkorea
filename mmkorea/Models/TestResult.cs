﻿using System;
namespace mmkorea.Models
{
    public class TestResult
    {
        public int Id { get; set; }

        public int Score { get; set; }

        public TestTaker Owner { get; set; }

        public School School { get; set; }

        public TestType TestType { get; set; }

    }

    public enum TestType
    {
        A,
        B,
        C,
        D
    }
}
