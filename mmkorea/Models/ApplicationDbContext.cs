﻿using System;
using Microsoft.EntityFrameworkCore;

namespace mmkorea.Models
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {

        }

        public DbSet<School> School { get; set; }

        public DbSet<User> User { get; set; }

        public DbSet<TestResult> TestResult { get; set; }

        public DbSet<TestTaker> TestTaker { get; set; }

    }
}
