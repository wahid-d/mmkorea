﻿using System;
using System.ComponentModel.DataAnnotations;

namespace mmkorea.Models
{
    public class TestTaker
    {
        public int Id { get; set; }

        public string Name { get; set; }

        [DataType(DataType.DateTime)]
        public DateTime DateOfBirth { get; set; }
    }
}
