using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using mmkorea.Models;

namespace mmkorea.Controllers
{
    public class TestTakerController : Controller
    {
        private readonly ApplicationDbContext _context;

        public TestTakerController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: TestTaker
        public async Task<IActionResult> Index()
        {
            return View(await _context.TestTaker.ToListAsync());
        }

        // GET: TestTaker/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var testTaker = await _context.TestTaker
                .FirstOrDefaultAsync(m => m.Id == id);
            if (testTaker == null)
            {
                return NotFound();
            }

            return View(testTaker);
        }

        // GET: TestTaker/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: TestTaker/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Name,DateOfBirth")] TestTaker testTaker)
        {
            if (ModelState.IsValid)
            {
                _context.Add(testTaker);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(testTaker);
        }

        // GET: TestTaker/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var testTaker = await _context.TestTaker.FindAsync(id);
            if (testTaker == null)
            {
                return NotFound();
            }
            return View(testTaker);
        }

        // POST: TestTaker/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name,DateOfBirth")] TestTaker testTaker)
        {
            if (id != testTaker.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(testTaker);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TestTakerExists(testTaker.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(testTaker);
        }

        // GET: TestTaker/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var testTaker = await _context.TestTaker
                .FirstOrDefaultAsync(m => m.Id == id);
            if (testTaker == null)
            {
                return NotFound();
            }

            return View(testTaker);
        }

        // POST: TestTaker/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var testTaker = await _context.TestTaker.FindAsync(id);
            _context.TestTaker.Remove(testTaker);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TestTakerExists(int id)
        {
            return _context.TestTaker.Any(e => e.Id == id);
        }
    }
}
